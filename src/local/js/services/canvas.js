(function () {

	angular
	  .module('R3.Services.Canvas', [])
	  .factory('Canvas', Canvas);
	  
	Canvas.$inject = ['Layers', 'Player'];

	function Canvas(Layers, Player) {

		console.log("Canvas service...");
	
		var canvasDiv = document.getElementById('canvasDiv');
		var canvasWidth = Player.getPlayerWidth() -2;
		var canvasHeight = Player.getPlayerHeight() - 45;

		canvas = document.createElement('canvas');
		canvas.setAttribute('width', canvasWidth);
		canvas.setAttribute('height', canvasHeight);
		canvas.setAttribute('id', 'canvas');
		canvasDiv.appendChild(canvas);

		if (typeof G_vmlCanvasManager != 'undefined') {
			canvas = G_vmlCanvasManager.initElement(canvas);
		}
		
		context = canvas.getContext("2d");

		var clickX = new Array();
		var clickY = new Array();
		var clickDrag = new Array();
		var paint;		
		
		var service = {
			init: init,
			animate: animate,
			redraw: redraw,
			clearCanvas: clearCanvas
		
		};
		
		return service;	

		// ----------------------------
		function continueAnimation() {
			var result = !window.paused && Player.getPlayer().getCurrentTime() < Player.getPlayer().getDuration();
			return result;
		}
		
		function clearCanvas() {
			context.clearRect(0, 0, context.canvas.width, context.canvas.height);
		}
		
		function animate() {
			clearCanvas();
			context.strokeStyle = "#df4b26";
			context.lineJoin = "round";
			context.lineWidth = 5;

			var j = Player.getPlayer().getCurrentTime();
			//for (var j = 0; j < player.getCurrentTime(); j++) {
						
			angular.forEach(Layers.getVisibleLayers(), function(v,i) {
				if( j >= v.minVal && j <= v.maxVal ) {
					for (var i = 0; i < v.clickX.length; i++) {
						context.beginPath();
						if (v.clickDrag[i] && i) {
							context.moveTo(v.clickX[i - 1], v.clickY[i - 1]);
						} else {
							context.moveTo(v.clickX[i] - 1, v.clickY[i]);
						}
						context.lineTo(v.clickX[i], v.clickY[i]);
						context.closePath();
						context.stroke();
					}
				}
			});		
			//}

			if (continueAnimation()) {
				requestAnimationFrame(animate);
			} else {
				console.log("Stop animation");
			}
		}
		
		function init() {
			console.log("Canvas.init()");
		}

		// This is needed so that we are able to draw on the canvas - we continuously
		// display what was just drawn
		function redraw() {
			context.clearRect(0, 0, context.canvas.width, context.canvas.height); // Clears the canvas
			context.strokeStyle = "#df4b26";
			context.lineJoin = "round";
			context.lineWidth = 5;

			var currentLayer = Layers.getActiveLayer();
			for (var i = 0; i < currentLayer.clickX.length; i++) {
				context.beginPath();
				if (currentLayer.clickDrag[i] && i) {
					context.moveTo(currentLayer.clickX[i - 1], currentLayer.clickY[i - 1]);
				} else {
					context.moveTo(currentLayer.clickX[i] - 1, currentLayer.clickY[i]);
				}
				context.lineTo(currentLayer.clickX[i], currentLayer.clickY[i]);
				context.closePath();
				context.stroke();
			}
		}
	
	}  

})();	  
