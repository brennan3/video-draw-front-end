(function () {

	angular
	  .module('R3.Services.Layers', [])
	  .factory('Layers', Layers);
	  
	Layers.$inject = [];

	function Layers() {
		
		var emptyLayer = {};
		emptyLayer.minVal = 0;
		emptyLayer.showLayer = true;
		emptyLayer.range = {
			min: 0
		};
		
		emptyLayer.clickX = new Array();
		emptyLayer.clickY = new Array();
		emptyLayer.clickDrag = new Array();
		
		var layers = [];
		
		var service = {
		
			getLayers: getLayers,
			getVisibleLayers: getVisibleLayers,
			addLayer: addLayer,
			deleteLayer: deleteLayer,
			getActiveLayer: getActiveLayer,
			setActiveLayer: setActiveLayer,
			addClick: addClick
		};
		
		return service;	

		// ----------------------------
		function addLayer(duration) {
			var newLayer = angular.copy(emptyLayer);
			newLayer.maxVal = duration;
			newLayer.range.max = duration;
			layers.push(newLayer);
			setActiveLayer(newLayer);
		}

		function deleteLayer(layer) {
			layers.splice(layers.indexOf(layer), 1);
		}
		
		function getVisibleLayers() {
			var filteredData = $.grep(layers, function(n, i) {
				return (n.showLayer);
			});
		
			return filteredData;
		}
		
		function getActiveLayer() {
			var filteredData = $.grep(layers, function(n, i) {
				return (n.isSelected === 1);
			});
		
			return filteredData[0];
		}
		
		function getLayers() {
			return layers;
		}

		function setActiveLayer(layer) {
			angular.forEach(layers, function(v,i) {
				if( v == layer) {
					v.isSelected = 1;
				} else {
					v.isSelected = 0;
				}
			});
		}
		
		function addClick(x, y, dragging) {
			//var videoTime = Math.floor(player.getCurrentTime());
			
			var currentLayer = getActiveLayer();
			currentLayer.clickX.push(x);
			currentLayer.clickY.push(y);
			currentLayer.clickDrag.push(dragging);

			//videoDrawings[videoTime].clickX.push(x)
			//videoDrawings[videoTime].clickY.push(y);
			//videoDrawings[videoTime].clickDrag.push(dragging);
		}
		
		
	}  

})();	  
