(function () {

	angular
	  .module('R3.Services.Player', [])
	  .factory('Player', Player);
	  
	Player.$inject = [];

	function Player() {
		
		console.log("Player Service...");
		
		var playerParams = {
			height: '390',
			width: '640',
			videoId: 'gths8z60j_w',
			events: {
				'onReady': onPlayerReady,
				'onStateChange': onPlayerStateChange
			}
		};
		
		// Load the IFrame Player API code asynchronously.
		var tag = document.createElement('script');
		tag.src = "https://www.youtube.com/iframe_api";
		
		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

		// Create an <iframe> (and YouTube player)
		//    after the API code downloads.
		var player;

		function onYouTubeIframeAPIReady() {
			console.log("Player is ready");
			player = new YT.Player('player', playerParams);
		}
		
		// Need to put this in global scope per the youtube api
		window.onYouTubeIframeAPIReady = onYouTubeIframeAPIReady;

		
		function onPlayerReady(event) {
			console.log(player);
		}

		function onPlayerStateChange(event) {
			if (event.data == YT.PlayerState.PLAYING) {
				window.paused = false;
				//setTimeout(stopVideo, 6000);
				//done = true;
			} else if (event.data == YT.PlayerState.PAUSED) {
				window.paused = true;
			}

			console.log("paused: " + window.paused);
			console.log("event data: " + event.data);
		}
		
		var service = {	
			getPlayer: getPlayer,
			getPlayerHeight: getPlayerHeight,
			getPlayerWidth: getPlayerWidth
		}  
		
		function getPlayer() {
			return player;
		}
		
		function getPlayerWidth() {
			return playerParams.width;
		}
		
		function getPlayerHeight() {
			return playerParams.height;
		}
		
		return service;
	}	

})();	  
