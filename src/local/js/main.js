// https://developers.google.com/youtube/youtube_player_demo

// ------------------------------------------------------------------------
// Video player
// ------------------------------------------------------------------------
/*
var playerParams = {
    height: '390',
    width: '640',
    videoId: 'gths8z60j_w',
    events: {
        'onReady': onPlayerReady,
        'onStateChange': onPlayerStateChange
    }
};

// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;

function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', playerParams);
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
    //prepVideoDrawingsArray();
    console.log(player);
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
//BUFFERING: 3
//CUED: 5
//ENDED: 0
//PAUSED: 2
//PLAYING: 1
//UNSTARTED: -1

var done = false;
var paused = true;

//function continueAnimation() {
    //var result = !paused && player.getCurrentTime() < player.getDuration();
    //var result = player.getCurrentTime() < player.getDuration();
    //console.log("Paused: " + paused);
    //console.log("player.getCurrentTime(): " + player.getCurrentTime());
    //console.log("player.getDuration(): " + player.getDuration());

    //console.log("REsult: " + result);
    //return result;
}


function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.PLAYING) {
        paused = false;
        //setTimeout(stopVideo, 6000);
        //done = true;
    } else if (event.data == YT.PlayerState.PAUSED) {
        paused = true;
    }

    console.log("paused: " + paused);
    console.log("event data: " + event.data);
}

// ----------------- Video Controls ----------------
function stopVideo() {
    player.stopVideo();
}

function play() {
    player.playVideo();
}

function pause() {
    player.pauseVideo();
}

*/



// ------------------------------------------------------------------------
// Drawing layer
// ------------------------------------------------------------------------
/*
var canvasDiv = document.getElementById('canvasDiv');
var canvasWidth = playerParams.width;
var canvasHeight = playerParams.height - 80;

canvas = document.createElement('canvas');
canvas.setAttribute('width', canvasWidth);
canvas.setAttribute('height', canvasHeight);
canvas.setAttribute('id', 'canvas');
canvasDiv.appendChild(canvas);

if (typeof G_vmlCanvasManager != 'undefined') {
    canvas = G_vmlCanvasManager.initElement(canvas);
}
context = canvas.getContext("2d");

var clickX = new Array();
var clickY = new Array();
var clickDrag = new Array();
var paint;

// Array of arrays, Each 1 second of video time is an array
// that contains all drawings for that moment in time of the video
var videoDrawings = new Array();

var prepVideoDrawingsArray = function() {
    console.log("Total Video Duration: " + player.getDuration());
    for (var i = 0; i < player.getDuration(); i++) {
        videoDrawings[i] = {};
        videoDrawings[i].clickX = new Array();
        videoDrawings[i].clickY = new Array();
        videoDrawings[i].clickDrag = new Array();
    }
}

function addClick(x, y, dragging) {
    var videoTime = Math.floor(player.getCurrentTime());
    //console.log();
    clickX.push(x);
    clickY.push(y);
    clickDrag.push(dragging);

    videoDrawings[videoTime].clickX.push(x)
    videoDrawings[videoTime].clickY.push(y);
    videoDrawings[videoTime].clickDrag.push(dragging);

}

function redraw() {
    context.clearRect(0, 0, context.canvas.width, context.canvas.height); // Clears the canvas

    context.strokeStyle = "#df4b26";
    context.lineJoin = "round";
    context.lineWidth = 5;

    for (var i = 0; i < clickX.length; i++) {
        context.beginPath();
        if (clickDrag[i] && i) {
            context.moveTo(clickX[i - 1], clickY[i - 1]);
        } else {
            context.moveTo(clickX[i] - 1, clickY[i]);
        }
        context.lineTo(clickX[i], clickY[i]);
        context.closePath();
        context.stroke();
    }
}

$('#canvas').mousedown(function(e) {
    //var mouseX = e.pageX - this.offsetLeft;
    var mouseX = e.pageX - 50;
    //var mouseY = e.pageY - this.offsetTop;
    var mouseY = e.pageY - 50;
    paint = true;
    addClick(mouseX, mouseY);
    redraw();
});

$('#canvas').mousemove(function(e) {
    if (paint) {
        var mouseX = e.pageX - 50;
        var mouseY = e.pageY - 50;
        addClick(mouseX, mouseY, true);
        redraw();
    }
});

$('#canvas').mouseup(function(e) {
    paint = false;
});

$('#canvas').mouseleave(function(e) {
    paint = false;
});

playVideoWithDrawing = function() {
    setTimeout(function() {
		paused = false;
        clearCanvas();
        player.seekTo(0);
        player.playVideo();
        requestAnimationFrame(animate);
    }, 250);
}

function clearCanvas() {
    context.clearRect(0, 0, context.canvas.width, context.canvas.height);
}

function animate() {
    // For each second of video play, display all drawings
    // that took place up to that point in time

    context.clearRect(0, 0, context.canvas.width, context.canvas.height); // Clears the canvas

    context.strokeStyle = "#df4b26";
    context.lineJoin = "round";
    context.lineWidth = 5;

    for (var j = 0; j < player.getCurrentTime(); j++) {
        for (var i = 0; i < videoDrawings[j].clickX.length; i++) {
            context.beginPath();
            if (videoDrawings[j].clickDrag[i] && i) {
                context.moveTo(videoDrawings[j].clickX[i - 1], videoDrawings[j].clickY[i - 1]);
            } else {
                context.moveTo(videoDrawings[j].clickX[i] - 1, videoDrawings[j].clickY[i]);
            }
            context.lineTo(videoDrawings[j].clickX[i], videoDrawings[j].clickY[i]);
            context.closePath();
            context.stroke();
        }
    }

    console.log("animate...");
    //console.log("Video Duration: " + player.getDuration());	
    //console.log("Video Time: " + Math.floor(player.getCurrentTime()));
    if (continueAnimation()) {
        requestAnimationFrame(animate);
    } else {
        console.log("Stop animation");
    }
}
*/