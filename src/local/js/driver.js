(function () {

	angular
	  .module('app', [
		'ngRoute',
		'R3.Modules.CreateVideo',
		'R3.Modules.Recent',
		'R3.Modules.About',
		'R3.Modules.PlayVideo',
		'R3.Services.Layers',
		'R3.Services.Player',
		'R3.Services.Canvas',
		'ui-rangeSlider'
		])
	  .config(config);
	  
	config.$inject = ['$locationProvider', '$routeProvider']; 
	  
	function config($locationProvider, $routeProvider) {
		$locationProvider.html5Mode(false);
		
		$routeProvider.when('/', {
			templateUrl: 'src/local/views/home.tpl.html'		
		});
		
		$routeProvider.otherwise({redirectTo:function(a, b, c){
			//return '/status/404?module=' + b;
			return '/';
		}});
		
	}

	angular
	  .module('app')
	  .controller('AppCtrl', AppCtrl);
	  
	AppCtrl.$inject = ['$scope', '$location', '$http', 'Player'];

	function AppCtrl($scope,  $location, $http, Player) {
		var vm = this;
		vm.mouseDown = mouseDown;
		vm.mouseMove = mouseMove;
		vm.hideCanvas = hideCanvas;
		vm.showCanvas = showCanvas;
		
		function mouseDown(event) {
			console.log("mouseDown: " + event.clientX + ", " + event.clientY);
			console.log(event);
			console.log(event.clientX);
			console.log(event.currentTarget);
			
		}
		
		function mouseMove(event) {
			console.log("mouseMove: " + event.clientX + ", " + event.clientY);
			
			//$(event.currentTarget).css("left", event.clientX);
			//$(event.currentTarget).css("top", event.clientY);
		}
		
		setTimeout(function(){
			//$scope.$apply(function(){
				//Canvas.init();
			//});
		}, 500);

		
		function showCanvas() {
			$('#create-video-board').css("display", "block");
		}
		
		function hideCanvas() {
			$('#create-video-board').css("display", "none");
		}
		
		
		
	}  

})();	  
