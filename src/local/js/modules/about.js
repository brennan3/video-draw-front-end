
(function () {

	angular
		.module('R3.Modules.About', [])
		.config(config);
	  
	config.$inject = ['$locationProvider', '$routeProvider']; 

	function config($locationProvider, $routeProvider) {
		$routeProvider.when('/about', {
			templateUrl: 'src/local/views/about.tpl.html'			
		});
	}

	
})();