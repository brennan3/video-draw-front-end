
(function () {

	angular
		.module('R3.Modules.Recent', [])
		.config(config);
	  
	config.$inject = ['$locationProvider', '$routeProvider']; 

	function config($locationProvider, $routeProvider) {
		$routeProvider.when('/recent', {
			templateUrl: 'src/local/views/recent.tpl.html',
			controller: 'RecentCtrl',
			controllerAs: 'RecentCtrl'
		});
	}
	
	angular
		.module('R3.Modules.Recent')	
		.controller('RecentCtrl', Recent);

	Recent.$inject = []; 
		
		
	function Recent() {
		console.log("R3.Modules.Recent.RecentCtrl: Controller Enter");

		var vm = this;
		
		
	}
	
})();