// http://danielcrisp.github.io/angular-rangeslider/
(function () {

	angular
		.module('R3.Modules.CreateVideo', [])
		.config(config);
	  
	config.$inject = ['$locationProvider', '$routeProvider']; 

	function config($locationProvider, $routeProvider) {
		$routeProvider.when('/create', {
			templateUrl: 'src/local/views/create.tpl.html',
			controller: 'CreateVideoCtrl',
			controllerAs: 'CreateVideoCtrl'
		});
	}
	
	angular
		.module('R3.Modules.CreateVideo')	
		.controller('CreateVideoCtrl', CreateVideo);

	CreateVideo.$inject = ['$scope', 'Canvas', 'Layers', 'Player']; 
		
	function CreateVideo($scope, Canvas, Layers, Player) {
		console.log("R3.Modules.CreateVideo.CreateVideoCtrl: Controller Enter");

		var vm = this;
		vm.addLayer = addLayer;
		vm.deleteLayer = deleteLayer;
		vm.setActiveLayer = setActiveLayer;
		vm.layers = Layers.getLayers();
		vm.playVideoWithDrawing = playVideoWithDrawing;
		vm.playVideo = playVideo;
		vm.stopVideo = stopVideo;
		vm.pauseVideo = pauseVideo;
	
		var videoDuration;
		
		function playVideo() {
			Player.getPlayer().playVideo();
		}
		
		function pauseVideo() {
			Player.getPlayer().pauseVideo();
		}
		
		function stopVideo() {
			Player.getPlayer().stopVideo();
		}
		
		function addLayer() {
			Layers.addLayer(videoDuration);
		}
		
		function deleteLayer(layer) {
			Layers.deleteLayer(layer);
			Canvas.clearCanvas();
		}

		function setActiveLayer(layer) {
			Layers.setActiveLayer(layer);
			Canvas.redraw();
		}
		
		function playVideoWithDrawing() {
			console.log("Preview");
			
			setTimeout(function() {
				window.paused = false;
				Canvas.clearCanvas();
				Player.getPlayer().seekTo(0);
				Player.getPlayer().playVideo();
				requestAnimationFrame(Canvas.animate);
			}, 250);
			
		}
		
		
		function clearCanvas() {
			Canvas.clearCanvas();
		}
		
		
		setTimeout(function(){
			$scope.$apply(function(){
				//console.log(player);
				videoDuration = Player.getPlayer().getDuration();
				addLayer();
			});
		}, 1500);

		
		$('#create-video-board').css("display", "block");
	
		
		// Event Listeners -------------------------------
		
		var paint;
		
		$('#canvas').mousedown(function(e) {
				console.log("mouse down");
		
			//var mouseX = e.pageX - this.offsetLeft;
			var mouseX = e.pageX - 50;
			//var mouseY = e.pageY - this.offsetTop;
			var mouseY = e.pageY - 50;
			paint = true;
			Layers.addClick(mouseX, mouseY);
			Canvas.redraw();
		});

		$('#canvas').mousemove(function(e) {
			if (paint) {
				var mouseX = e.pageX - 50;
				var mouseY = e.pageY - 50;
				Layers.addClick(mouseX, mouseY, true);
				Canvas.redraw();
			}
		});

		$('#canvas').mouseup(function(e) {
			paint = false;
		});

		$('#canvas').mouseleave(function(e) {
			paint = false;
		});
		
	}
	
	
	angular
		.module('R3.Modules.CreateVideo')
		.filter('minAndSeconds', minAndSeconds);

	function minAndSeconds() {
	  return function(input) {
		var r = input % 60;
		var result;
		if(input < 60) {
			result = "0:";
			if( input < 10 ) {
				result += "0" + input;
			} else {
				result += input;
			}
			return result;
		} 
		else {
			result = Math.floor(input / 60) + ":";
			if( r < 10 ) {
				result += "0" + r;
			} else {
				result += r;
			}
			return result; 
		}
	  };
	}
	
	
})();