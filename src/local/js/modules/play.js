
(function () {

	angular
		.module('R3.Modules.PlayVideo', [])
		.config(config);
	  
	config.$inject = ['$locationProvider', '$routeProvider']; 

	function config($locationProvider, $routeProvider) {
		$routeProvider.when('/play/:id', {
			templateUrl: 'src/local/views/play.tpl.html',
			controller: 'PlayVideoCtrl',
			controllerAs: 'PlayVideoCtrl'
		});
		
		$routeProvider.when('/play', {
			templateUrl: 'src/local/views/play.tpl.html',
			controller: 'PlayVideoCtrl',
			controllerAs: 'PlayVideoCtrl'
		});
	}
	
	angular
		.module('R3.Modules.PlayVideo')	
		.controller('PlayVideoCtrl', PlayVideo);

	PlayVideo.$inject = []; 
		
		
	function PlayVideo() {
		console.log("R3.Modules.PlayVideo.PlayVideoCtrl: Controller Enter");

		var vm = this;
		
		
	}
	
})();