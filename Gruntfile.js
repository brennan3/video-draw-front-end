module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('config/package.json'),
        banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
            '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
            '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
            '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
            ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',

        // JS Minify
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build: {
                src: '<%= concat.dist.dest %>',
                dest: 'dist/<%= pkg.name %>.min.js'
            }
        },

        jsObfuscate: {
            test: {
                options: {
                    concurrency: 2,
                    keepLinefeeds: false,
                    keepIndentations: false,
                    encodeStrings: true,
                    encodeNumbers: true,
                    moveStrings: true,
                    replaceNames: true,
                    variableExclusions: ['^_get_', '^_set_', '^_mtd_']
                },
                files: {
                    'dist/obfuscated.js': [
                        '<%= concat.dist.dest %>'
                    ]
                }
            }
        },

        // JS Obfuscator
        obfuscator: {
            files: [
                //'dist/**/*.js'
                '<%= concat.dist.dest %>'
            ],
            entry: '<%= concat.dist.dest %>',
            out: 'dist/obfuscated.js',
            strings: true,
            root: __dirname
        },

        jshint: {
            options: {
                jshintrc: 'config/.jshintrc',
                errorsOnly: true,
                failOnError: false
            },
            all: [
                'GruntFiles.js',
                'src/**/*.js'
            ]
        },


        jslint: { // configure the task
            client: {
                src: [
                    'src/**/*.js'
                ],
                directives: {
                    browser: true,
                    predef: [
                        'jQuery'
                    ]
                },
                options: {
                    // junit: 'out/client-junit.xml'
                }
            }
        },

        // HTML pretty
        prettify: {
            options: {
                config: 'config/.jsbeautifyrc'
            },
            html: {
                // Target-specific file lists and/or options go here.
            },
            all: {
                expand: true,
                cwd: 'src/ugly/',
                ext: '.tpl.html',
                src: ['*tpl.html'],
                dest: 'src/ugly/'
            }
        },

        // JS Pretty
        jsbeautifier: {
            files: ["package.json", "Gruntfile.js", "src/**/*.js"],
            options: {}
        },

        // Merge JS files into single file
        concat: {
            options: {
                banner: '<%= banner %>',
                stripBanners: true
            },
            dist: {
                src: ['src/**/*.js'],
                dest: 'dist/<%= pkg.name %>.js'
            },
        },

        // Refresh our distribution
        clean: {
            src: ['dist']
        },
        qunit: {
            files: ['test/**/*.html']
        },
        watch: {
            src: {
                files: '<%= jshint.all %>',
                //tasks: ['jshint:all']
                tasks: ['jsbeautifier']
            } //,
            //test: {
            //   files: '<%= jshint.test.src %>',
            //  tasks: ['jshint:test', 'qunit']
            //},
        }


    });

    // These plugins provide necessary tasks
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    //grunt.loadNpmTasks('grunt-contrib-qunit');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch'); // https://github.com/gruntjs/grunt-contrib-watch
    grunt.loadNpmTasks('grunt-jslint');
    grunt.loadNpmTasks('grunt-continue'); // https://www.npmjs.org/package/grunt-continue
    grunt.loadNpmTasks('grunt-prettify'); // https://github.com/jonschlinkert/grunt-prettify // html prettifier
    grunt.loadNpmTasks('grunt-jsbeautifier'); // https://www.npmjs.org/package/grunt-jsbeautifier // js prettifier
    grunt.loadNpmTasks('grunt-obfuscator'); // https://www.npmjs.org/package/grunt-obfuscator
    grunt.loadNpmTasks('js-obfuscator'); //https://www.npmjs.org/package/js-obfuscator

    // Default task(s).
    // prettify
    // beautify
    grunt.registerTask('default', ['continueOn', 'uglify', 'jslint', 'jshint', 'jsbeautifier', 'prettify', 'continueOff']);
    grunt.registerTask('build', ['continueOn', 'clean', 'concat', 'uglify', 'jsObfuscate', 'continueOff']);
    grunt.registerTask('pretty', ['jsbeautifier']);


};
