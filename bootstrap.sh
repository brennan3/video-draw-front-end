#!/usr/bin/env bash

# sudo /etc/init.d/apache2 stop
# sudo /etc/init.d/apache2 start

sudo apt-get update

# ---- fix locale issue ----
sudo apt-get -y install language-pack-en 
sudo apt-get update


echo ">>> Installing Base Items..."

# Install base items
sudo apt-get install -y curl wget build-essential python-software-properties python g++ make

echo ">>> Adding PPA's and Installing Server Items..."

# Add NodeJS repository
sudo add-apt-repository -y ppa:chris-lea/node.js

# Update Again
sudo apt-get update
	
# Install the Rest
sudo apt-get install -y apache2 nodejs git

echo ">>> Configuring Server..."

# Apache Config
sudo a2enmod rewrite
sudo curl https://gist.github.com/fideloper/2710970/raw/5d7efd74628a1e3261707056604c99d7747fe37d/vhost.sh > vhost
sudo chmod guo+x vhost
sudo mv vhost /usr/local/bin

sudo ln -sf /vagrant /var/www/html/video-draw

sudo service apache2 restart

echo ">>> Installing NPM, Grunt CLI and Bower"

curl https://npmjs.org/install.sh | sh
sudo npm install -g grunt-cli bower



